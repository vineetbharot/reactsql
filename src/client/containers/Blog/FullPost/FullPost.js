import React, { Component } from 'react';
import axios from 'axios';

import moment from 'moment'

import './FullPost.css';

class FullPost extends Component {
    state = {
        loadedPost: null
    }

    componentDidMount () {
        console.log(this.props);
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    loadData () {
        if ( this.props.match.params.id ) {
            if ( !this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== this.props.match.params.id) ) {
                axios.get( '/employee?key=' + this.props.match.params.id )
                    .then( response => {
                        console.log('ladData', response);
                        if (response.data.result && response.data.result[0] && response.data.result[0].doc && response.data.result[0].doc._id) {
                            this.setState( { loadedPost: { ...response.data.result[0].doc, id: response.data.result[0].doc._id } } );
                        }
                    } );
            }
        }
    }

    deletePostHandler = () => {
        console.log('deletepost id', this.props.match.params.id);
        axios.delete('/employee', {
            data: { 
                id: this.props.match.params.id 
            }
        } )
            .then(response => {
                console.log('deletepost res', response);
            });
    }

    updatePostHandler = () => {
        const {
            name = this.state.loadedPost.name,
            dob = this.state.loadedPost.dob,
            salary = this.state.loadedPost.salary
        } = this.state;
        const data = {
            id: this.props.match.params.id,
            name,
            dob,
            salary,
            skills: []
        };
        axios.put( '/employee', data )
            .then( response => {
                console.log( response );
                this.props.history.replace('/posts');
                // this.setState( { submitted: true } );
            } );
    }

    render () {
        let post = <p style={{ textAlign: 'center' }}>Please select a Post!</p>;
        if ( this.props.match.params.id ) {
            post = <p style={{ textAlign: 'center' }}>Loading...!</p>;
        }
        if ( this.state.loadedPost ) {
            post = (
                <div className="FullPost">
                    <h1>{this.state.loadedPost.name}</h1>
                    <input type="text"  onChange={( event ) => this.setState( { name : event.target.value } ) } placeholder={this.state.loadedPost.name} />
                    <input type="text"  onChange={( event ) => this.setState( { dob : event.target.value } ) } placeholder={moment(this.state.loadedPost.dob).format('YYYY-MM-DD')} />
                    <input type="text"  onChange={( event ) => this.setState( { salary : event.target.value } ) } placeholder={this.state.loadedPost.salary} />
                    <div className="Edit">
                        <button onClick={this.updatePostHandler} className="Update">Update</button>
                    </div>
                    <h1></h1>
                    <div className="Edit">
                        <button onClick={this.deletePostHandler} className="Delete">Delete</button>
                    </div>
                </div>

            );
        }
        return post;
    }
}

export default FullPost;