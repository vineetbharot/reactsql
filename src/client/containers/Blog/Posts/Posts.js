import React, { Component } from 'react';
import axios from '../../../axios';
import { Route } from 'react-router-dom';

import Post from '../../../components/Post/Post';
import './Posts.css';
import FullPost from '../FullPost/FullPost';

class Posts extends Component {
    state = {
        posts: []
    }

    componentDidMount () {
        console.log( this.props );
        axios.get( '/employee' )
            .then( response => {
                const posts = response.data.result;
                console.log( 'response', response );
                const updatedPosts = posts.map( post => {
                    return {
                        ...post.doc,
                        id: post.doc._id
                    }
                } );
                this.setState( { posts: updatedPosts } );
            } )
            .catch( error => {
                console.log( error );
                // this.setState({error: true});
            } );
    }

    postSelectedHandler = ( id ) => {
        // this.props.history.push({pathname: '/posts/' + id});
        this.props.history.push( '/posts/' + id );
    }

    deletePostHandler = ( id ) => {
        console.log('deletepost id', id);
        axios.delete('/employee', {
            data: { 
                id: id 
            }
        } )
            .then(response => {
                console.log('deletepost res', response);
            });
    }

    render () {
        let posts = <p style={{ textAlign: 'center' }}>Something went wrong!</p>;
        if ( !this.state.error ) {
            posts = this.state.posts.map( post => {
                return (
                    // <Link to={'/posts/' + post.id} key={post.id}>
                    <Post
                        key={post.id}
                        name={post.name}
                        dob={post.dob}
                        clicked={() => this.postSelectedHandler( post.id )} 
                        deleted={() => this.deletePostHandler( post.id )}
                        />
                    // </Link>
                );
            } );
        }

        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <Route path={this.props.match.url + '/:id'} exact component={FullPost} />
            </div>
        );
    }
}

export default Posts;