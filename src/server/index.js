const express = require('express');
const os = require('os');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.raw({ limit: '500mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '500mb' }));
app.use(express.static('dist'));

const models = require('./models');

models.sequelize.sync()
  .then((seqSyncRes) => {
    console.log('seqSyncRes', !!seqSyncRes);
  })
  .catch((seqSyncErr) => {
    console.error('seqSyncErr', seqSyncErr.message)
  })

app.use('/', require('./routes'));

// error handler
app.use((err, req, res, next) => {
  console.trace('END err', err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.listen(8080, () => console.log('Listening on port 8080!'));
