const express = require('express');

const helper = require('../../utils/helper');

const errors = require('../../server/config/errors');

const router = express.Router();

const Driver = require('../models').Driver;
const Request = require('../models').Request;

const create = async (req, res, next) => {
    try {
        const driverCreateRes = await Driver.create({
            name: req.param('name'),
            status: 'available'
        });
        return helper.handleResponse(req, res, null, driverCreateRes);
    } catch (error) {
        return helper.handleResponse(req, res, error);
    }
};

const getById = async (req, res, next) => {
    try {
        const getRes = await Driver.findById(req.params.id);
        return helper.handleResponse(req, res, null, getRes);
    } catch (error) {
        return helper.handleResponse(req, res,  error);
    }
};

const serveRequest = async (req, res, next) => {
    try {
        const requestId = req.body.requestId;
        if (!requestId) {
            throw errors.missingParam('requestId');
        }
        const [driver, request] = await Promise.all([
            Driver.findById(req.params.id),
            Request.findById(requestId)
        ]);

        console.log('driver reqeust', JSON.stringify(driver, null, 4), JSON.stringify(request, null, 4));
        // if (!request || request.status != 'pending') {
        //     throw new Error('invalid request');
        // } else if (!driver || driver.status != 'available') {
        //     throw new Error('driver unavailable');
        // }
        const updateRes = Promise.all([
            driver.update({
                status: 'serving'
            }),
            request.update({
                status: 'ongoing'
            })
        ]);
        console.log('updateRes', JSON.stringify(updateRes, null, 4));

        setTimeout(async () => {
            console.log('here')
            try {
                const finalUpdateRes = await Promise.all([
                    driver.update({
                        status: 'available'
                    }),
                    request.update({
                        status: 'completed'
                    })
                ]);
                // console.log('finalUpdateRes', finalUpdateRes);
                return finalUpdateRes;
            } catch (error) {
                console.error('error', error);
                return error;
            }
        }, 300000);

        return helper.handleResponse(req, res, null, updateRes);
    } catch (error) {
        console.log('finalErr', error);
        return helper.handleResponse(req, res,  error);        
    }
}

router.get('/:id', getById);
router.post('/:id/serve', serveRequest);
router.post('/', create);

module.exports = router;