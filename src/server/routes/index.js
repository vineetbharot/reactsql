const express = require('express');

const router = express.Router();

router.use('/driver', require('./driver'));
router.use('/request', require('./request'));

module.exports = router;