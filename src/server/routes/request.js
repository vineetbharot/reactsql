const express = require('express');

const helper = require('../../utils/helper');

const router = express.Router();

const Request = require('../models').Request;

const create = async (req, res, next) => {
    try {
        const RequestCreateRes = await Request.create({
            status: 'pending'
        });
        return helper.handleResponse(req, res, null, RequestCreateRes);
    } catch (error) {
        return helper.handleResponse(req, res, error);
    }
};

const getById = async (req, res, next) => {
    try {
        const getRes = await Request.findById(req.params.id)
        return helper.handleResponse(req, res, null, getRes);
    } catch (error) {
        return helper.handleResponse(req, res,  error)
    }
}

router.get('/:id', getById);
router.post('/', create);


module.exports = router;