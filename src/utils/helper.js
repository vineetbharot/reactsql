const bluebird = require('bluebird');
const uuid = require('uuid');
const errors = require('../server/config/errors');
const constants = require('../server/config/constants');
const moment = require('moment');

const asyncResponse = function (callbackFunc, err, data) {
  if (typeof callbackFunc === 'function') {
    callbackFunc(err, data);
  }
  if (err) {
    return Promise.reject(err);
  }
  return Promise.resolve(data);
};

const sendResponse = function (promiseFunc, callbackFunc, err, data) {
  if (err) {
    err.status = err.status || err.statusCode;
    err.statusCode = err.statusCode || err.status;
    if (typeof callbackFunc === 'function') {
      callbackFunc(err);
    } else {
      promiseFunc(err);
    }
    return;
  }
  if (typeof callbackFunc === 'function') {
    callbackFunc(null, data);
  } else {
    promiseFunc(data);
  }
};``

const handleResponse = function (req, res, err, response) {
  if (err) {
    return res.json({
      status: 'error',
      ok: false,
      code: err.statusCode || 400,
      message: err.message || err,
      result: err.message
    });
  }
  return res.json({
    status: 'success',
    ok: true,
    code: 200,
    message: '',
    result: response,
  });
};

const promisifyDB = function (db, callback) {
  return new Promise((resolve, reject) => {
    Promise.all([bluebird.promisifyAll(db), bluebird.promisifyAll(db.attachment), bluebird.promisifyAll(db.multipart)])
      .then(results => this.sendResponse(resolve, callback, null, results))
      .catch(err => this.sendResponse(reject, callback, err));
  });
};


module.exports = {
  asyncResponse,
  handleResponse,
  promisifyDB,
  sendResponse,
};